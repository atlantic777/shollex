from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from datetime import date, datetime, timedelta

class Employee(models.Model):
    job             = models.ForeignKey('Job')
    uuid            = models.CharField(blank=True,max_length=10)
    modes           = models.ManyToManyField('Mode', through='Entry')
    user            = models.OneToOneField(User)

    def can_logout(self):
        try:
            entry = Entry.objects.filter(user=self).order_by('-login').all()[0]
            login_delta = datetime.now() - entry.login
            if entry.is_logged_out() is False and (login_delta.seconds/3600. > 16 or login_delta.days > 0):
                return False
            else:
                return True
        except:
            return False

    def worktime_for_month(self, year=date.today().year, month=date.today().month):
        entry_list = Entry.objects.filter(login__month=month, login__year=year, user=self, mode__is_worktime=True)
        sum_in_seconds = sum([e.worktime_delta().total_seconds() for e in entry_list])
        delta = timedelta(0,sum_in_seconds)
        return delta

    def worktime_for_month_h(self, year, month):
        delta = self.worktime_for_month(year, month)
        return ""+str(int(delta.total_seconds())//3600)+":"+str((int(delta.total_seconds())//60)%60)+":"+str(int(delta.total_seconds()%60))


    def worktime_for_day(self, year=date.today().year, month=date.today().month, day=date.today().day):
        entry_list = Entry.objects.filter(login__month=month, login__year=year, login__day=day, user=self, mode__is_worktime=True)
        sum_in_seconds = sum([e.worktime_delta().total_seconds() for e in entry_list])
        delta = timedelta(0,sum_in_seconds)
        return delta

    def worktime_for_day_h(self, year, month, day):
        delta = self.worktime_for_day(year, month, day)
        return ""+str(int(delta.total_seconds())//3600)+":"+str((int(delta.total_seconds())//60)%60)+":"+str(int(delta.total_seconds()%60))


    #FIXME monthly worktime per modes (for reports)

    def is_logged_out(self):
        try:
            entry = Entry.objects.filter(login__contains=date.today(), user=self).order_by('-login')[0]
            if entry.logout is not None:
                return True
            else:
                return False
        except:
            return True

    def __unicode__(self):
        return self.user.first_name+' '+self.user.last_name

    def was_present(self, given_date):
        entry_list = Entry.objects.filter(login__contains=given_date, user=self).all()
        if len(entry_list) == 0:
            return False
        else:
            return True

class Mode(models.Model):
    name            = models.CharField(max_length=20)
    description     = models.TextField()
    requires_info   = models.BooleanField(default=False)
    is_worktime     = models.BooleanField(default=True)

    def __unicode__(self):
        return self.name

class Entry(models.Model):
    login           = models.DateTimeField()
    logout          = models.DateTimeField(null=True,blank=True)
    user            = models.ForeignKey('Employee')
    mode            = models.ForeignKey('Mode')

    def worktime_delta(self):
        if self.logout is None:
            return timedelta(0)
        else:
            return self.logout-self.login

    def worktime_delta_s(self):
        s = str(self.worktime_delta())
        return s.split('.')[0]

    def worktime_delta_h(self):
        delta = self.worktime_delta()
        return ""+str(int(delta.total_seconds())//3600)+":"+str((int(delta.total_seconds())//60)%60)+":"+str(int(delta.total_seconds()%60))


    def info(self):
        try:
            return AdditionalInfo.objects.filter(entry=self).all()[0].info
        except:
            return ""

    def is_logged_out(self):
        if self.logout:
            return True
        else:
            return False

    is_logged_out.boolean = True

    @staticmethod
    def get_years_list():
        q = set([e['login'].year for e in Entry.objects.values('login').all()])
        q = list(q)
        return q

    @staticmethod
    def get_months_for_year(selected_year):
        q = set([e['login'].month for e in Entry.objects.filter(login__year=selected_year).values('login').order_by('login').all()])
        q = list(q)[::-1]
        return q

    class Meta:
        verbose_name_plural = 'Entries'

class Job(models.Model):
    name            = models.CharField(max_length=10)
    description     = models.TextField()

    def __unicode__(self):
        return self.name

class AdditionalInfo(models.Model):
    entry   = models.OneToOneField(Entry)
    info    = models.TextField()

class EmployeeInline(admin.StackedInline):
    model = Employee
    can_delete = False
    verbose_name = 'employee'

class AdditionalInfoInline(admin.StackedInline):
    model = AdditionalInfo
    can_delete = False
