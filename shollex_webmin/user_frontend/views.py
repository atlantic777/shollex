# Create your views here.
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.http import HttpResponse

@login_required
def home(request):
    return render(request, 'index.html')

def logout_view(request):
    logout(request)
    return HttpResponse('Logout successfull. <a href="/">homepage</a>')
