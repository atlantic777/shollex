# Create your views here.
from django.shortcuts import render
from django.template import Context
from datetime import date, datetime
from calendar import monthrange, month_name
from backend.models import *

def report(request):
    return render(request, "report.html")

def monthly(request, year, month):
    """
    get month and year
    get date
    get users list
    render shit
    """
    table_data = {}

    current_year = int(year)
    current_month = int(month)

    """
    entry     = Entry.objects.all()[0]
    last_emp  = entry.user
    """
    days_header_list = range(1, monthrange(current_year, current_month)[1]+1)
    days_date_list = [date(current_year, current_month, day) for day in days_header_list]

    table_data['month'] = [current_month, month_name[current_month]]
    table_data['year'] = year

    table_data['days'] = days_header_list
    table_data['user_info'] = []

    for last_emp in Employee.objects.order_by("user__first_name").all():
        presence_list = [last_emp.was_present(day) for day in days_date_list]

        table_data['user_info'].append((last_emp,presence_list, last_emp.worktime_for_month_h(current_year, current_month)))

    return render(request, 'monthly.html', Context({'table_data':table_data}))

def per_person(request, year, month, uid):
    """
    get person
    get month
    get date list
    get entries per day
    append entries per day to tmp list
    append per day lists to result list
    make object and render
    """
    month = int(month)
    year = int(year)

    employee = Employee.objects.get(id=uid);
    entries_list = Entry.objects.filter(user=employee, login__month=month, login__year=year).all()

    days_list = set([e.login.day for e in entries_list])
    dates_list = [date(year, month, day) for day in days_list]

    result_list = []

    for current_date in dates_list:
        tmp_dict = {'date':current_date,'entries':[], 'total':employee.worktime_for_day_h(year, month, current_date.day),}
        current_date_entries = [e for e in entries_list if e.login.date() == current_date]
        for entry in current_date_entries:
            tmp_dict['entries'].append(entry)
        result_list.append(tmp_dict)

    return render(request, 'per_person.html', Context({'data':result_list}))

def report2(request):
    return render(request, 'report2.html')
