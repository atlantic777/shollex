function hide_fields() {
    $("#button_container").hide('fast');
    $("#user_info").hide('fast');
    $("#user_logins").hide('fast');
    $("#logout_container").html('');
}

function show_fields() {
    $("#button_container").show('slow');
    $("#user_info").show('slow');
    $("#user_logins").show('slow');
}

function reset_all() {
    hide_fields();
    $("#token_input").val('');
    $("#token_input").focus();

    /*
    $("#button_container").hide();
    $("#user_info").hide();
    $("#user_logs").hide();
    $("#logout_container").html('');
    */
}

function show_popup() {
    var content = $("#pageID");
    $('<div id="popup" data-role="popup" data-theme="b" data-position-to="window">'+
        '<p>Uspešno dodat unos</p>'+
        '<a href="#" data-rel="back" data-role="button" data-theme="a"'+
        'data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a></div>').appendTo(content);
    $("#pageID").trigger('create');
    $("#popup").bind({
        popupafterclose : function(event,ui) {$("#token_input").focus();},
        popupafteropen  : function(event,ui) {setTimeout(function(){$("#popup").popup('close');}, 5000)},
    });
    $("#popup").popup('open');
}

function get_info(e) {
    var token = e.data.token;
    var mode_name = e.data.mode_name;

    $( document ).delegate("#info", "pageshow", function() {
        var button_container = $("#dialog_buttons");
            button_container.html('');

        var ok_button = $('<a data-role="button">Dodaj zapis</a>');
            ok_button.appendTo(button_container);
            ok_button.on('click', function() {
                $.post('/api/add_entry/'+token+'/'+mode_name+'/',
                    {'info':$("#additional_info_area").val()});
                $.mobile.changePage("#pageID", {role:'page'});
                show_popup();
                reset_all();
            });

        $("#info").trigger('create');

        $("#additional_info_area").focus();
    });

    $.mobile.changePage("#info", {
        role: 'dialog',
    });
}

function add_entry_ajax()
{
    var token = $("#token_input").val();

    $.ajax( {
        url     : '/api/add_entry/'+token+'/'+$(this).data('url')+'/',
        success : function(data, textStatus, XMLHttpRequest) {
                        show_popup();
                        reset_all();
                    },
        });
}

function token_is_valid(check) {
    if( check == true) {
        $("#notifications").html('<p>Kartica je uspešno očitana.');
        $("#notifications").append("<p>Izaberite radno mesto.");
        return 1;
    }
    else {
        $("#notifications").html('<p>Korisnik nije pronađen');
        return 0;
    }
}

function update_mode_buttons(mode_list) {
    var button_list = $("#button_container");

    button_list.html('<li data-role="list-divider">Radno mesto</li>');

    $(function() {
        $.each(mode_list, function(num, mode) {
            var list_item = $('<li></li>')
            var button = $('<a data-url="'+mode[0]+'">'+mode[0]+'</a>');
                button.appendTo(list_item)

            if (mode[1] === false)
                button.on('click', add_entry_ajax);
            else
                button.on('click', {token:$("#token_input").val(),mode_name:mode[0]}, get_info);

            list_item.appendTo(button_list);
        });
    });

    button_list.listview('refresh');
}

function update_user_logins(login_list) {
    var entry_container = $("#user_logins");
    var c = null;

    entry_container.html('<li data-role="list-divider">Prijavljivanja</li>');

    $(function() {
        $.each(login_list, function(num, entry) {
            if(entry[3] == null)
                c = 'red';
            else
                c = 'green';

            html_string =  $('<li id="'+c+'"><h3>'+entry[0]+'</h3><p>'+entry[2]+'</p>'+
                             '<p>'+entry[3]+'</p><h2 class="ui-li-aside">'+entry[1]+'</h2></li>');
            entry_container.append(html_string);
        });
    });

    entry_container.listview('refresh');
}

function update_user_info(user) {
    var user_container = $("#user_info");

    user_container.html('<li data-role="list-divider">Vaši podaci</li>');
    user_container.append('<li>Ime i prezime <h2 class="ui-li-aside">'+user.first_name+' '+user.last_name+'</h2></li>');
    user_container.append('<li>Zaposlenje <h2 class="ui-li-aside">'+user.job+'</h2></li>');
    user_container.append('<li>Ovog meseca <h2 class="ui-li-aside">'+user.workhours+'</h2></li>');
    user_container.append('<li>Danas <h2 class="ui-li-aside">'+user.workhours_today+'</h2></li>');

    user_container.listview('refresh');
}

function update_logout(logged_out) {
    console.log(logged_out);

    if(logged_out == false){
        var container = $("#logout_container");
            container.html('');

        var button = $('<a id="logout" data-role="button" data-url="logout">Odjavi se!</a>');
            button.appendTo(container).on('click', add_entry_ajax);

        container.trigger('create');
    }
}

function token_check()
{
    var registerSuccess = function(data, textStatus, XMLHttpRequest)
    {
        try
        {
            var obj = JSON.parse(data);

            if(token_is_valid(obj.check) == 0) return -1;

            update_mode_buttons(obj.mode_list);
            update_user_logins(obj.entry_list);
            update_user_info(obj.user);
            update_logout(obj.logged_out);

            show_fields();
        }
        catch(error)
        {
            $("#notifications").text("Error parsing JSON");
        }
    };

    $.ajax({data: "", dataType:"text", success: registerSuccess, type:"GET",
        url:"/api/token_check/"+$("#token_input").val()+'/'});
}


function token_input_handler() {
    setTimeout( function() {
            var token_len = $("#token_input").val().length;

            if (token_len  == 10 )
            {
                token_check();
            }
            else {
                hide_fields();

                if(token_len == 0)
                    $("#notifications").html("<p>Očitajte karticu");
                else
                    $("#notifications").html("<p>Nedovoljan broj karaktera.<p>Opet očitajte karticu");
            }
        }, 200);
}

$(document).ready(function() {
    $("#token_input").on('keyup', token_input_handler);
    $("#token_input").on('change', token_input_handler);

    token_input_handler();

    $("#token_input").focus();
});
