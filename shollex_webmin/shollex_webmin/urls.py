from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from user_frontend.views import logout_view

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'shollex_webmin.views.home', name='home'),
    # url(r'^shollex_webmin/', include('shollex_webmin.foo.urls')),
     url(r'^$', 'user_frontend.views.home'),
     url(r'^report/$', 'admin_frontend.views.report'),
     url(r'^report/monthly/(?P<year>[0-9]+)/(?P<month>[0-9]+)/$', 'admin_frontend.views.monthly'),
     url(r'^report/detailed/(?P<year>[0-9]+)/(?P<month>[0-9]+)/(?P<uid>[0-9]+)/$', 'admin_frontend.views.per_person'),

    # Uncomment the admin/doc line below to enable admin documentation:
     url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^admin/', include(admin.site.urls)),
     url(r'^api/', include('api.urls')),
     url(r'^logout/', logout_view),
     url(r'^accounts/login/$', 'django.contrib.auth.views.login'),
)
