function load_report() {
    var year = $("#select-year :selected").text();
    var month = $("#select-month :selected").text();

    $("#report-container").load("/report/monthly/"+year+"/"+month+"/ #table");
}
function populate_year_selector() {
    $.get("/api/years_list/", function(data) {
        populate_month_selector(data['years'][0]);
        $("#select-year").html('');
        $.each(data['years'], function(num,year) {
            current_html_string = '<option value="'+year+'">'+year+'</option>';
            $("#select-year").append(current_html_string);
            $("#select-year").selectmenu('refresh');
        });
    });
};

function populate_month_selector(year) {
    $.get("/api/months_list/"+year+"/", function(data) {
        $("#select-month").html('')
        $.each(data['months'], function(num, month) {
            current_html_string = '<option value="'+month+'">'+month+'</option>';
            $("#select-month").append(current_html_string);
            $("#select-month").selectmenu('refresh');
        });
    });
}

$("#load-report").click(load_report);


$(document).on('swipeleft',function() {
    $("#panelID").panel('open');
});

$(document).ready(function() {
    $("#panelID").panel('open');
    $("#select-year").on('change', function(){
        var year = $("#select-year :selected").text();
        populate_month_selector(year);
    });
});

$("#panelID").on('panelbeforeopen',function() {
    populate_year_selector();
});
