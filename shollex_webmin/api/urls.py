from django.conf.urls import *

urlpatterns = patterns('',
        url(r'token_check/(?P<uuid>([0-9])+)/$', 'api.views.token_check'),
        url(r'add_entry/(?P<uuid>([0-9])+)/(?P<mode_name>(.*))/$', 'api.views.add_entry'),
        url(r'years_list/$', 'api.views.years_list'),
        url(r'months_list/(?P<year>[0-9]+)/$', 'api.views.months_list'),
        )
