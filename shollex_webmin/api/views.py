# Create your views here.
from django.shortcuts import render_to_response, render
from django.http import HttpResponse
from backend.models import User, Mode, Entry, Employee, AdditionalInfo
from datetime import datetime
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

import json
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt

def timedelta2str(delta):
    return str(delta).split('.')[0]

@login_required
def years_list(request):
    json_response = json.dumps({ 'years':Entry.get_years_list(), })
    return HttpResponse(json_response, mimetype='application/json')

def months_list(request, year):
    json_response = json.dumps({ 'months':Entry.get_months_for_year(year),})
    return HttpResponse(json_response, mimetype='application/json')


@login_required
def token_check(request, uuid):
    employee = Employee.objects.filter(uuid=uuid).all()
    mode_list = Mode.objects.all()

    if len(employee) is 1:
        employee = employee[0]
        user     = employee.user
    else:
        employee = None
        user     = None

    entry_list = Entry.objects.filter(user=employee).order_by("-login").all()[:5]

    json_response = ''

    dthandler = lambda obj: str(obj.replace(microsecond=0)) if isinstance(obj, datetime) else None

    if user is not None:
        json_response = json.dumps({'check':True,
            'user': {
                'first_name': user.first_name,
                'last_name' : user.last_name,
                'workhours' : str(employee.worktime_for_month()),
                'workhours_today' : str(employee.worktime_for_day()),
                'job'       : employee.job.name },
                'mode_list' : [(mode.name, mode.requires_info) for mode in mode_list ],
                'entry_list': [(e.mode.name, timedelta2str(e.worktime_delta()),e.login, e.logout) for e in entry_list],
                'logged_out': employee.is_logged_out(),
                'can_logout': employee.can_logout(),
            }, default=dthandler);
    else:
        json_response = json.dumps({'check':False})
    return HttpResponse(json_response, mimetype='application/json')

@login_required
@csrf_exempt
def add_entry(request, uuid, mode_name):
    user = Employee.objects.filter(uuid=uuid).all()
    user = user[0]
    entry = None

    if mode_name != 'logout':
        mode = Mode.objects.filter(name=mode_name).all()
        mode = mode[0]

        try:
            last_entry = Entry.objects.filter(user=user).order_by('-login').all()[0]
            if last_entry.is_logged_out() is False and user.can_logout() is True:
                last_entry.logout = datetime.now()
                last_entry.save()
        except:
            pass

        entry = Entry()
        entry.login =  datetime.now()
        entry.user = user
        entry.mode = mode
        entry.save()

        if mode.requires_info:
            info_text = request.POST['info']
            info = AdditionalInfo(info=info_text)
            info.entry = entry
            info.save()

    else:
        if user.can_logout() is True:
            entry = Entry.objects.filter(user=user).order_by('-login').all()[0]
            entry.logout = datetime.now().replace(microsecond=0)
            entry.save()
        else:
            pass

    return HttpResponse(entry.id)
