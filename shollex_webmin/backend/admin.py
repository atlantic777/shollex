from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm
from backend.models import *

class UserAdmin(UserAdmin):
    def job(obj):
        return obj.employee.job
    def uuid(obj):
        return obj.employee.uuid

    add_fieldsets = (
            (None, {
                'classes': ('wide',),
                'fields': ('username', 'password1', 'password2', 'first_name', 'last_name')}
                ),
            )

    inlines = (EmployeeInline,)
    list_display = ('username','first_name', 'last_name', job, uuid, 'email','is_staff',)
    list_display_links = ('username','first_name', 'last_name',)

class ModeAdmin(admin.ModelAdmin):
    list_display    = ('name', 'description', 'requires_info','is_worktime',)

class JobAdmin(admin.ModelAdmin):
    list_display    = ('name', 'description')

class EntryAdmin(admin.ModelAdmin):
    list_display    = ('user', 'mode','login','logout', 'worktime_delta', 'is_logged_out', 'info')
    list_filter     = ('user', 'mode')
    list_display_links = ('login', 'logout')
    inlines = (AdditionalInfoInline,)

admin.site.unregister(User)

admin.site.register(User, UserAdmin)
admin.site.register(Mode, ModeAdmin)
admin.site.register(Entry,EntryAdmin)
admin.site.register(Job, JobAdmin)
